extends NinePatchRect


signal spin
signal stop_spinning
signal set_score


# переменные позиции футера (показать/спрятать)
var show_pos:Vector2
var hide_pos:Vector2

var min_bet = 0.25 # минимальный порог ставки
var max_bet = 5 # максимальный порог ставки
var bet_step = 0.25 # шаг инкремента/дикремента
var bet = min_bet # текущий коэффициент ставки

var spinning = false # состояние спина
var auto_spinning = false # состояние автоспина
var auto_spin_max = 15.0 # количество повторов автоспина

var spin_time_max = 4 # максимальное время кручения спина
var spin_timer = 0 # таймер кручения спина


# отображение прогресса автоспина. increase: boolean
func show_hide_spin_progress(increase: bool) -> void:
	var offset = Vector2(0.0, 100.0) # отступ для сдвига всего прогресса (спрятать/показать)
	var pos = $ProgressBarBg.rect_position
	var spin_progress_p = 0 # уровень шкалы
	if increase: # если true, то показываем прогресс
		spin_progress_p = pos - offset
	else: # если false, то прячем прогресс
		spin_progress_p = pos + offset
	$Tween.interpolate_property($ProgressBarBg, "rect_position", pos, spin_progress_p, 0.2, Tween.TRANS_SINE)

var auto_spin_count = 0.0 # счетчик количества повторов автоспина
# автоспин
func auto_spin() -> void:
	var size = $ProgressBarBg/ProgressBarFg.rect_size
	var to:Vector2
	if auto_spin_max > auto_spin_count: # если меньше максимального количества спинов, то продолжаем крутить
		auto_spin_count += 1.0 # увеличиваем счетчик
		spin_timer = 0
		spinning = true
		to = Vector2(lerp(42, 241, auto_spin_count / auto_spin_max), size.y)
	else: # если закончили, то останавливаем автоспины
		spinning = false
		auto_spinning = false
		auto_spin_count = 0.0
		to = Vector2(42.0, size.y)
		show_hide_spin_progress(false)
	# увеличиваем шкалу прогресса автоспинов
	$Tween.interpolate_property($ProgressBarBg/ProgressBarFg, "rect_size", size, to, 0.2, Tween.TRANS_SINE)
	if not $Tween.is_active():
		$Tween.start()

func _process(delta: float) -> void:
	$Bet/BetLabel.text = str(round(bet * 100) / 100) + "$"
	if spinning:
		spin_timer += 70 * delta
		if spin_timer > spin_time_max:
			emit_signal("spin")
			spin_timer = 0

func _input(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.is_action("auto_spin") and event.doubleclick:
		if not spinning and not auto_spinning: 
			spinning = true
			auto_spinning = true
			show_hide_spin_progress(true)
			$TimerSpin.start()
			auto_spin()

func _ready() -> void:
	show_pos = self.rect_position
	hide_pos = self.rect_position + Vector2(0.0, 150.0)
	show_hide_spin_progress(false)
	$TimerSpin.stop()
	$TimerPause.stop()


func _on_Main_show_footer(show: bool) -> void:
	if show:
		$Tween.interpolate_property(self, "rect_position", hide_pos, show_pos, 0.2, Tween.TRANS_SINE)
	else:
		$Tween.interpolate_property(self, "rect_position", show_pos, hide_pos, 0.2, Tween.TRANS_SINE)
	if not $Tween.is_active():
		$Tween.start()


func _on_BetMinusBtn_button_down() -> void:
	if spinning and auto_spinning: return
	var old_bet = bet
	bet = bet - bet_step if bet > min_bet else min_bet
	$Tween.interpolate_property(self, "bet", old_bet, bet, 0.13)
	if not $Tween.is_active():
		$Tween.start()


func _on_BetPlusBtn_button_down() -> void:
	if spinning and auto_spinning: return
	var old_bet = bet
	bet = bet + bet_step if bet < max_bet else max_bet
	$Tween.interpolate_property(self, "bet", old_bet, bet, 0.13)
	if not $Tween.is_active():
		$Tween.start()


func _on_MaxBetBtn_button_down() -> void:
	if spinning and auto_spinning: return
	var old_bet = bet
	bet = max_bet
	$Tween.interpolate_property(self, "bet", old_bet, bet, 0.13)
	if not $Tween.is_active():
		$Tween.start()


func _on_SpinBtn_button_down() -> void:
	if spinning and auto_spinning: return
	spinning = true
	$TimerSpin.start()


func _on_TimerSpin_timeout() -> void:
	if spinning: 
		emit_signal("stop_spinning")
		emit_signal("set_score")
		spinning = false
		if auto_spinning:
			$TimerPause.start()


func _on_TimerPause_timeout() -> void:
	auto_spin()
	$TimerSpin.start()
