extends TextureRect

export (PackedScene) var Icon


var level = 0
var lvl = [
	# level 1
	{
		# ширина и высота иконки
		ic_w = 200,
		ic_h = 150,
		# отступы между иконками
		ic_gap_w = 64,
		ic_gap_h = 5,
		# сдвиг иконок внутри доски. если null, то центрируется из центра
		ic_offset_x = null,
		ic_offset_y = 20,
		# сдвиг доски внутри экрана. если null, то центрируется из центра
		brd_offset_x = null,
		brd_offset_y = 90,
		# количество иконок горизонтально и вертикально
		ic_count_x = 3,
		ic_count_y = 3,
		ic_variations = 5 # количество иконок в сете
		# preloaded_textures = [Texture, ...] — при инициализации подгружаются текстуры
	},
	# level 2
	{
		ic_w = 170,
		ic_h = 150,
		ic_gap_w = 40,
		ic_gap_h = 5,
		ic_offset_x = null,
		ic_offset_y = null,
		brd_offset_x = null,
		brd_offset_y = 85,
		ic_count_x = 4,
		ic_count_y = 3,
		ic_variations = 5
	},
	# level 3
	{
		ic_w = 160,
		ic_h = 150,
		ic_gap_w = 8,
		ic_gap_h = 18,
		ic_offset_x = null,
		ic_offset_y = null,
		brd_offset_x = null,
		brd_offset_y = 85,
		ic_count_x = 5,
		ic_count_y = 3,
		ic_variations = 5
	}
]


# отображение выбранного уровня
func show_level() -> void:
	self.set_texture(load("res://assets/image/bg_{lvl}.png".format({"lvl": level + 1})))
	$Board.set_texture(load("res://assets/image/board_{lvl}.png".format({"lvl": level + 1})))
	$Board.rect_size = Vector2(0.0, 0.0) # HACK: чтобы сбрасывать размер контейнера после изменения текстуры
	$Board.rect_position = (get_viewport_rect().size - $Board.rect_size) / 2
	if lvl[level].brd_offset_x != null:
		$Board.rect_position.x = lvl[level].brd_offset_x
	if lvl[level].brd_offset_y != null:
		$Board.rect_position.y = lvl[level].brd_offset_y
	spin_timer = spin_time_max


var spin_time_max = 4 # максимальное время кручения спина
var spin_timer = 4 # таймер кручения спина

# уставнока слотов (иконок)
func set_slots() -> void:
	var ic_texture: Object
	var vars: int = lvl[level].ic_variations
	for slot in get_tree().get_nodes_in_group("icons_group"):
		ic_texture = lvl[level].preloaded_textures[randi() % vars]
		slot.set_texture(ic_texture)

# создание сетки слотов. l — таблица с параметрами текущего уровня
func set_slot_grid() -> void:
	var l = lvl[level]
	var size = $Board.rect_size
	var o_x: float
	var o_y: float
	# если не задан оффсет, то сетка по центру
	o_x = (size.x - (l.ic_w * l.ic_count_x + l.ic_gap_w * (l.ic_count_x - 1))) / 2 if l.ic_offset_x == null else l.ic_offset_x
	o_y = (size.y - (l.ic_h * l.ic_count_y + l.ic_gap_h * (l.ic_count_y - 1))) / 2 if l.ic_offset_y == null else l.ic_offset_y
	# отрисовка сетки
	for i in l.ic_count_x:
		var p_x = o_x + (l.ic_w + l.ic_gap_w) * i + 1
		for j in l.ic_count_y:
			var p_y = o_y + (l.ic_h + l.ic_gap_h) * j + 1
			var child = Icon.instance()
			child.rect_position = Vector2(p_x, p_y)
			$Board.add_child(child)
			
func select_level(_level) -> void:
	level = _level
	for icon in get_tree().get_nodes_in_group("icons_group"):
		icon.queue_free()
	show_level()
	set_slot_grid()
	set_slots()


func _ready() -> void:
	var path: String
	# кеширование текстур
	for i in lvl.size():
		var variantions = lvl[i].ic_variations
		lvl[i].preloaded_textures = []
		for j in range(0, variantions):
			path = "res://assets/image/ic_{lvl}_{vars}.png".format({"lvl": i + 1, "vars": j + 1})
			lvl[i].preloaded_textures.append(load(path))
		
	randomize()
	select_level(level)


func _on_Main_show_level(_level) -> void:
	select_level(_level)
	
func _on_SpinBtn_spin() -> void:
	for slot in get_tree().get_nodes_in_group("icons_group"):
		slot.set_blur(true)
	set_slots()

func _on_Spinning_stop() -> void:
	for slot in get_tree().get_nodes_in_group("icons_group"):
		slot.set_blur(false)
