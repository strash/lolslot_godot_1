extends NinePatchRect


export (PackedScene) var BigCount

signal show_menu


var score = 10000.0 # очки
var level = 1.0 # уровень
var level_score = 0.0 # выигранные очки
var level_step = 150.0 # шаг уровня
var chanse_to_win = 10 # шанс на победу. условно в процентах
var rate = 15.0 # повышающий коэффициент при выигрыше


# установка уровня. count: int
func set_level(count) -> void:
	level_score = level_score + count
	level = (level_score - fmod(level_score, level_step)) / level_step + 1.0
	$CounterLevel/LabelCount.text = str(level_score) + "/" + str(level * level_step)
	$CounterLevel/LabelLevel.text = str(level)

# изменение счета. increase: boolean
func set_score(increase: bool) -> void:
	var bet = get_parent().get_node("Footer").bet
	var big_count = BigCount.instance()
	big_count.rect_position = Vector2(150.0, 300.0)
	if increase: # выиграли
		var count = floor(rate * bet + level) # сумма выигрыша/проигрыша
		score = score + count
		set_level(count)
		big_count.increase = true
		big_count.count = count
	elif not increase: # проиграли
		score = score - bet
		big_count.increase = false
		big_count.count = bet
	get_parent().add_child(big_count)
	# пишем результат в интерфейс
	$CounterScore/LabelCount.text = str(score) + "$"


func _ready() -> void:
	$CounterLevel/LabelCount.text = "0/" + str(score)
	$CounterLevel/LabelLevel.text = str(level)
	$CounterScore/LabelCount.text = str(score) + "$"



func _on_BackBtn_button_down() -> void:
	var footer = get_parent().get_node("Footer")
	if not footer.spinning and not footer.auto_spinning:
		emit_signal("show_menu")


func _on_Footer_spinning() -> void:
	var bet = get_parent().get_node("Footer").bet
	# шанс выигрыша зависит от ставки. чем выше ставка, тем выше шанс выигрыша
	if randi() % 100 <= chanse_to_win + chanse_to_win * bet / 2:
		set_score(true)
	else:
		set_score(false)
	
