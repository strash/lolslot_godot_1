extends Label



var increase = false
var count = 0

var green = Color(0.0 / 255.0, 175.0 / 255.0, 0.0 / 255.0)
var red = Color(185.0 / 255.0, 0.0 / 255.0, 122.0 / 255.0)



func _ready() -> void:
	var pos = self.rect_position
	var new_pos: Vector2
	var new_pos_x = rand_range(pos.x - 20.0, pos.x + 20.0)
	if increase:
		self.text = "+" + str(count) + "$"
		self.modulate = green
		new_pos = Vector2(new_pos_x, rand_range(pos.y - 80.0, pos.y - 30.0))
	else:
		self.text = "-" + str(count) + "$"
		self.modulate = red
		new_pos = Vector2(new_pos_x, rand_range(pos.y + 30.0, pos.y + 80.0))
	$Tween.interpolate_property(self, "rect_position", pos, new_pos, 1.5, Tween.TRANS_SINE)
	$Tween.start()


func _on_Timer_timeout() -> void:
	queue_free()
