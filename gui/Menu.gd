extends Control


signal select_level(level)


func _ready() -> void:
	pass


func _on_Level1_button_down(extra_arg_0: int) -> void:
	emit_signal("select_level", extra_arg_0)
	self.hide()


func _on_Level2_button_down(extra_arg_0: int) -> void:
	emit_signal("select_level", extra_arg_0)
	self.hide()


func _on_Level3_button_down(extra_arg_0: int) -> void:
	emit_signal("select_level", extra_arg_0)
	self.hide()
