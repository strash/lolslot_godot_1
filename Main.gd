extends Node


signal show_level(level)
signal show_footer(show)


func _on_Menu_select_level(level) -> void:
	emit_signal("show_level", level)
	emit_signal("show_footer", true)
	$Header/BackBtn.show()

func _on_BackBtn_show_menu() -> void:
	emit_signal("show_footer", false)
	$Header/BackBtn.hide()
	$Menu.show()

func _ready() -> void:
	var _s_select_level = $Menu.connect("select_level", self, "_on_Menu_select_level")
	var _s_show_level = self.connect("show_level", $Level, "_on_Main_show_level")
	var _s_show_menu = $Header.connect("show_menu", self, "_on_BackBtn_show_menu")
	var _s_show_footer = self.connect("show_footer", $Footer, "_on_Main_show_footer")
	var _s_spin = $Footer.connect("spin", $Level, "_on_SpinBtn_spin")
	var _s_spin_stop = $Footer.connect("stop_spinning", $Level, "_on_Spinning_stop")
	var _s_set_score = $Footer.connect("set_score", $Header, "_on_Footer_spinning")
	
	$Header/BackBtn.hide()
	emit_signal("show_footer", false)
